
  
import re

def clean(cpf: str) -> str:
    return re.sub(r'\D', '', cpf)

def has_correct_length(cpf: str) -> bool:
    CPF_VALID_SIZE = 11
    return len(cpf) == CPF_VALID_SIZE

def is_allowed(cpf: str) -> bool:
    first_digit = cpf[0]
    return cpf.count(first_digit) != len(cpf)

def caculate_digit(cpf: str, is_first_digit: bool = True) -> int:
    FACTORY = 10 if is_first_digit else 11
    FACTORIES = range(FACTORY, 1, -1)
    result_sum: int = sum([factory * int(cpf[index]) for index, factory in enumerate(FACTORIES)])
    REST: int = result_sum % 11
    if REST < 2 or REST > 10:
        return 0
    return 11 - REST

def is_valid(cpf: str) -> bool:
    cpf: str = clean(cpf)
    if not has_correct_length(cpf):
        return False
    if not is_allowed(cpf):
        return False
    first_verification_digit: int = caculate_digit(cpf)
    last_verification_digit: int = caculate_digit(cpf, is_first_digit=False)
    return cpf[-2:] == f'{first_verification_digit}{last_verification_digit}'