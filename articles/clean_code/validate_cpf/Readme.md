# Como validar um CPF

Pra validar um CPF é necessário calcular os dígitos verificadores a partir dos nove primeiros digitos do mesmo.

Exemplo:
Para o CPF 111.444.777-35
a base para o cálculo é 111444777

**Importante** (somente caracteres numéricos)

## Calculando o primeiro dígito verificador

- Para calcular o primeiro dígito, mulipicamos cada caractere separadamente pelo número correspondente a sua posição começando do 10 e indo de forma decrescente sempre até o 2.

    Parece complicado mais não é, trazendo isso para o python isso basicamente significa que teremos duas listas:

    1) A primeira lista nada mais é do que um `split` da base do cpf

    2) A segunda é apenas uma lista decrescente de 10 a 2.

Conforme o exemplo abaixo:

```python

primeira_lista = '111444777'.split('')
# [1, 1, 1, 4, 4, 4, 7, 7, 7]

segunda_lista = [10, 9, 8, 7, 6, 5, 4, 3, 2]
# [10, 9, 8, 7, 6, 5, 4, 3, 2]
```

*Note que ambas as listas possuem o mesmo tamanho.*

Precisamos multiplicar  cada digito do CPF pelo respectivo número e salvar o resultado de cada multiplicação em uma nova lista, da seguinte forma:

CPF base **111444777**

| 1  | 1 | 1 | 4 | 4 | 4 | 7 | 7 | 7 |
|---|---|---|---|---|---|---|---|----|
| x  | x | x | x | x | x | x | x | x |
| 10 | 9 | 8 | 7 | 6 | 5 | 4 | 3 | 2 |
| =  | = | = | = | = | = | = | = | = |
| 10 | 9 | 8 | 28| 24| 20| 28| 21| 14 |


```python
resultado = [10, 9, 8, 28, 24, 20, 28, 21, 14]
```

Agora somamos todos os resultados da etapa anterior:
`10 + 9 + 8 + 28 + 24 + 20 + 28 + 21 + 14 = 162`
e calculamos o resto da divisão por 11
`162 % 11 = 8`

Com isso já podemos descobrir o primeiro dígito verificador.

* Se o resto da divisão por 11 for menor que 2 o dígito verificador é igual a **0 (Zero)**.

* Se o resto da divisão por 11 for maior que 10 o dígito verificador é igual a **0 (Zero)**.

* Senão for nenhuma das anteriores o dígito verificador é igual a 11 - o resto da divisão.
Para o nosso exemplo `11 - 8 = 3`, logo o primeiro dígito é **3**.

## Calculando o segundo dígito verificador

O cálculo do segundo dígito verificador é realizado da mesma forma que o primeiro, a única diferença é que agora temos como base 10 dígitos, os 9 primeiros + o digito verificador da etapa anterior:

9 primeiros: `111444777`
1° dígito verificador `3`

base `1114447773`

Com isso agora temos a lista decrescente iniciando em 11 novamente indo até 2:

```python

primeira_lista = '1114447773'.split('')
# [1, 1, 1, 4, 4, 4, 7, 7, 7, 3]

segunda_lista = [11, 10, 9, 8, 7, 6, 5, 4, 3, 2]
# [11, 10, 9, 8, 7, 6, 5, 4, 3, 2]
```

*Novamente note que ambas as listas possuem o mesmo tamanho.*


| 1  | 1  | 1 | 4 | 4 | 4 | 7 | 7 | 7 | 3 |
|--- |--- |---|---|---|---|---|---|---|---|
| x  | x  | x | x | x | x | x | x | x | x |
| 11 | 10 | 9 | 8 | 7 | 6 | 5 | 4 | 3 | 2 |
| =  | =  | = | = | = | = | = | = | = | = |
| 11 | 10 | 9 | 32| 28| 24| 35| 28| 21| 6 |

```python
resultado = [11, 10, 9, 32, 28, 24, 35, 28, 21, 6]
```
Somamos todos os resultados:
`11 + 10 + 9 + 32 + 28 + 24 + 35 + 28 + 21 + 6 = 204`
e calculamos o resto da divisão por 11
`204 % 11 = 6`

Novamente já podemos descobrir o segundo dígito verificador, utilizando as mesmas regras.

* Se o resto da divisão por 11 for menor que 2 o dígito verificador é igual a **0 (Zero)**.

* Se o resto da divisão por 11 for maior que 10 o dígito verificador é igual a **0 (Zero)**.

* Senão for nenhuma das anteriores o dígito verificador é igual a 11 - o resto da divisão.
Para o nosso exemplo `11 - 6 = 5`, logo o segundo dígito é **5**.

## Conclusão

Descobrimos que o primeiro digito verificador é igual a **3**
e que o segundo é igual a **5**.

O que corresponde ao cpf informado **111.444.777-35**, logo podemos dizer que é um CPF válido.

[fonte](https://www.macoratti.net/alg_cpf.htm)