
## Referêcias

[![Livro](https://a-static.mlcdn.com.br/618x463/livro-codigo-limpo/saraiva/2666082/195af3474eb4a670a48ed1c19ce0342a.jpg)](https://www.google.com.br/books/edition/C%C3%B3digo_Limpo/GXWkDwAAQBAJ?hl=pt-BR&gbpv=1&printsec=frontcover)

Uma citação muito conhecida do Martin Fowler diz:

**“Qualquer tolo consegue escrever código que um computador entenda. Bons programadores escrevem código que humanos possam entender”**.


### Código limpo é:

* Código limpo é legível: Use nomes curtos e precisos para variáveis, classes e funções. É ideal que essas práticas sejam definidas e adotadas pela equipe. <br/>

*  Código limpo é praticado: A melhor maneira de aprender a escrever código limpo é fazer isso o tempo todo. Quando você está em casa trabalhando em um projeto pessoal ou estudando, faça isso com código limpo.<br/>

*  Código limpo é simples: Quanto menos, melhor! Truques excessivamente inteligentes, hacks e truques são divertidos para o autor e diminuem o valor a longo prazo do código. O mesmo vale para o código prolixo que leva uma eternidade para chegar ao ponto.<br/>

*  Código limpo é testado: Ninguém escreve código perfeito e sem erros na primeira tentativa. Um código sem testes não dá garantia de que o código não será quebrado depois. Escrever código limpo significa escrever código testado. Dessa forma, os futuros desenvolvedores poderão fazer alterações possuindo um conjunto de testes pronto para confirmar que nada quebrou.<br/>

*  Código limpo é implacavelmente refatorado: Código limpo deve estar em um estado constante de refatoração.

### Code Smell
Um **Code Smell** não significa que algo está definitivamente errado, ou que algo deve ser corrigido imediatamente. É uma regra que deve alertá-lo para uma possível oportunidade de melhorar alguma coisa.

Os desenvolvedores normalmente procuraram por erros lógicos que foram introduzidos acidentalmente em seus códigos. Esses erros geralmente são bugs lógicos que causam a falha de sistemas inteiros.

Mas e os outros problemas que não afetam o funcionamento do sistema?
Por exemplo, os problemas de design que dificultam a manutenção do sistema e aumentam a chance de erros no futuro, etc.
Os Code Smells são sinais de que seu código deve ser refatorado para melhorar a legibilidade e a capacidade de suporte.

Identificar um código ruim não irá fazer com que você escreva um código limpo, mas reconhecer que você fez um código ruim e pode melhorá-lo é um ótimo início. Escrever código limpo exige prática, para iniciar essa prática é recomendável escrever testes unitários junto com seu código, assim você poderá refatorar seu código até que esteja o mais próximo possível de um código limpo.

### Fontes

[Medium](https://inside.contabilizei.com.br/c%C3%B3digo-limpo-d100db463e62)

![Vida de programador](https://miro.medium.com/max/1020/1*t121ld2jBNm_szHX_wmbfw.png)