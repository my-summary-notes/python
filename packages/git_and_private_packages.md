# Installing packages from github/gitlab...


#### Using HTTPS

Example **`package_url`**: [`github.com/drummerzzz/pypi_validate_cpf`](https://github.com/drummerzzz/pypi_validate_cpf)


* To install a python package from git use
**`pip install git+https://`** + **`package_url`**

Making the full path we have:

```bash
pip install git+https://github.com/drummerzzz/pypi_validate_cpf
```


#### Using SSH

Example **`package_url`**:
`git@github.com/drummerzzz/pypi_validate_cpf`

* To install a python package from git use
**`pip install git+ssh://`** + **`package_url`**

Making the full path we have:

```bash
pip install git+ssh://git@github.com/drummerzzz/pypi_validate_cpf
```


#### Using acess key token

* GitHub

`pip install git+https://${GITHUB_TOKEN}@github.com/user/project.git@{version}`

* GitLab

`pip install git+https://${GITLAB_TOKEN_USER}:${GITLAB_TOKEN}@gitlab.com/user/project.git@{version}`

* Bitbucket

`pip install git+https://${BITBUCKET_USER}:${BITBUCKET_APP_PASSWORD}@bitbucket.org/user/project.git@{version}`
